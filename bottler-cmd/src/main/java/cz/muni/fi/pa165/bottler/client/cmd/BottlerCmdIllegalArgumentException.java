package cz.muni.fi.pa165.bottler.client.cmd;

/**
 *
 * @author Vit Stanislav <373843@mail.muni.cz>
 */
class BottlerCmdIllegalArgumentException extends IllegalArgumentException {

    BottlerCmdIllegalArgumentException(String string) {
        super(string);
    }
}
